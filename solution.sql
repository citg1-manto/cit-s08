mysql -u root -p
-- Select/Use a database;
USE music_store;

-- 1. Find all artist that has letter D in its name.
SELECT * FROM artists WHERE name LIKE "%D%";

-- 2. Find all songs that has a length of less than 230
SELECT * FROM songs WHERE length < 230;

-- 3. Join the 'albums' and 'songs' tables.(Only Show the album name, song name, and song length) 
SELECT album_title, song_name, length FROM albums
JOIN songs ON songs.album_id = albums.id
JOIN artists ON artists.id = albums.artist_id;

-- 4. Join the 'artists' and 'albums' tables.(Find all albums that has letter A in its name).
SELECT * FROM albums
JOIN artists ON artists.id = albums.artist_id AND albums.album_title LIKE "%A%";

-- 5. Sort the albums in Z-A order. (Show only the first 4 records.)
SELECT * FROM albums ORDER BY album_title DESC LIMIT 4;

-- 6. Join the 'albums' and 'songs' tables. (Sort albumes from Z-A and sort songs from A-Z.)
SELECT * FROM albums
JOIN songs ON songs.album_id = albums.id 
ORDER BY albums.album_title DESC, songs.song_name ASC;

-- AUTHOR: FRANCIS JASPER P. MANTO
           